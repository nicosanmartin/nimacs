alias sudo="sudo "
alias emq="emacs -Q -nw"
alias emn="emacs -nw "

alias gmail="env HOME=$HOME/gnus-mails/gmail emacs -l '~/gnus-mail.el' --eval '(gnus-gmail)'"

alias dcmail="env HOME=$HOME/gnus-mails/dcmail emacs -l '~/gnus-mail.el' --eval '(gnus-dc)'"

